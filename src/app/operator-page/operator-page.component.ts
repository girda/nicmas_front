import {Component, OnInit} from '@angular/core';
import {AgGridService} from '../shared/services/ag-grid.service';
import * as moment from 'moment';
import {ButtonAgGridComponent} from '../shared/components/button-ag-grid/button-ag-grid.component';
import {IOperatorModal} from '../shared/interfaces/operator-modal.interface';
import {ModalService} from '../shared/components/modal/modal.service';
import {RestService} from '../shared/services/rest.service';
import {MaterialService} from '../shared/services/material.service';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-operator-page',
  templateUrl: './operator-page.component.html',
  styleUrls: ['./operator-page.component.scss']
})
export class OperatorPageComponent implements OnInit {
  dataModal: IOperatorModal;
  isSelected = false;

  constructor(public gridService: AgGridService,
              private modalService: ModalService,
              private rest: RestService) {
    this.gridService.gridOptions.columnDefs = this.createColumnDefs();
    this.gridService.gridOptions.frameworkComponents = {
      btnOpenModal: ButtonAgGridComponent,
    };
    this.gridService.gridOptions.context = { componentParent: this };
    this.gridService.init(`${environment.apiUrl}/api/operator`);
  }

  ngOnInit(): void {
  }

  submitRecordLabel() {
    const dataRecord = this.gridService.gridOptions.api.getSelectedRows()[0];
    this.rest.post(`${environment.apiUrl}/api/record`, dataRecord).subscribe(
      res => {
        this.gridService.gridOptions.api.setRowData(res.products);
        MaterialService.toast(res.message);
      },
      error => {
        console.log(error);
      }
    );
  }

  openModal(params) {
    this.dataModal = params.data;
    this.modalService.open();
  }

  onSelectionChanged() {
    const isSelection = this.gridService.gridOptions.api.getSelectedRows().length;
    this.isSelected = !!isSelection;
  }

  createColumnDefs() {
    return [
      {
        headerName: 'Name',
        field: 'name',
        checkboxSelection: true,
        sortable: true
      },
      {
        headerName: 'Client',
        field: 'client',
        sortable: true
      },
      {
        headerName: 'Code',
        field: 'code',
        sortable: true
      },
      {
        headerName: 'Date',
        field: 'createdAT',
        valueFormatter: (params) => {
          return moment(params.value).format('DD MMM, YYYY');
        },
        sortable: true
      },
      {
        headerName: '',
        cellRenderer: 'btnOpenModal',
        filter: false
      }
    ];
  }



}
