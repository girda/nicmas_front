import { Component, OnInit } from '@angular/core';
import { BarcodeFormat } from '@zxing/library';
@Component({
  selector: 'app-controller-page',
  templateUrl: './controller-page.component.html',
  styleUrls: ['./controller-page.component.scss']
})
export class ControllerPageComponent implements OnInit {

  allowedFormats = [BarcodeFormat.QR_CODE, BarcodeFormat.EAN_13, BarcodeFormat.CODE_128, BarcodeFormat.DATA_MATRIX];
  qrResultString: string;
  constructor() { }

  ngOnInit(): void {
  }

  onCodeResult(resultString: string) {
    this.qrResultString = resultString;
    // alert(this.qrResultString);
  }
}
