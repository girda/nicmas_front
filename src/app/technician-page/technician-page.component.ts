import { Component, OnInit } from '@angular/core';
import {TechnicianService} from '../shared/services/technician.service';
import {IProduct} from '../shared/interfaces/product.interface';

@Component({
  selector: 'app-technician-page',
  templateUrl: './technician-page.component.html',
  styleUrls: ['./technician-page.component.css']
})
export class TechnicianPageComponent implements OnInit {
  product: IProduct;
  constructor(private techService: TechnicianService) { }

  ngOnInit(): void {
    this.product = this.techService.getProduct();
    console.log(this.product);
  }

}
