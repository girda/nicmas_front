import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Subscription} from 'rxjs/internal/Subscription';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MaterialService} from '../../services/material.service';
import {KeysService} from '../../services/keys.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  form: FormGroup;
  aSub: Subscription;


  constructor(private auth: AuthService,
              private user: UserService,
              private router: Router,
              private route: ActivatedRoute,
              private keys: KeysService) {

  }

  ngOnInit() {
    this.form = new FormGroup({
      login: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(3)])
    });

    this.route.queryParams.subscribe((params: Params) => {
      if (params.registered) {
        MaterialService.toast('Тепер вы можете зайти в систему используя свои данные');
      } else if (params.accessDenied) {
        MaterialService.toast('Для начала нужно зарегистрироваться в системе');
      } else if (params.sessionFailed) {
        MaterialService.toast('Пожалуйста войдите в систему заного');
      } else if (params.incorrectRole) {
        MaterialService.toast('У Вас нет прав доступа');
      }
    });
  }

  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  onSubmit() {
    this.form.disable();

    this.aSub = this.auth.login(this.form.value).subscribe(
      () => {
        switch (this.user.getRole()) {
          case `${this.keys.role.operator}`:
            this.router.navigate(['/operator']);
            break;
          case `${this.keys.role.controller}`:
            this.router.navigate(['/controller']);
            break;
          case `${this.keys.role.technician}`:
            this.router.navigate(['/technician']);
            break;
        }
        console.log(this.user.getRole());
      },
      error => {
        if (error.error.message) {
          MaterialService.toast(error.error.message);
        } else {
          MaterialService.toast('Сервер не отвечает');
        }
        this.form.enable();
      }
    );

  }



}
