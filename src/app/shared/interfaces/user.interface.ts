export interface IUser {
  login: string;
  password: string;
  getRole?(role: string);
  setRole?(role: string);
}
