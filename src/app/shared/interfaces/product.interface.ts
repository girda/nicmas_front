export interface IProduct {
  id: number;
  name: string;
  code: string;
  client: number;
  number_invoice: string;
  date_invoice: string;
  cost_invoice: string;
  createdAT: string;
  username: string;
}
