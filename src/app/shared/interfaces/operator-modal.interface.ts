export interface IOperatorModal {
  name: string;
  client: number;
  code: string;
  cost_invoice: string;
  createdAT: string;
  date_invoice: string;
  id: number;
  number_invoice: string;
  username: string;
}
