import {Injectable} from '@angular/core';
import {RestService} from './rest.service';
import {GridOptions} from 'ag-grid-community';
import * as moment from 'moment';
import {log} from 'util';
@Injectable({
  providedIn: 'root'
})

export class AgGridService {

  gridOptions: GridOptions;
  constructor(private rest: RestService) {
    this.gridOptions = {
      defaultColDef: {
        width: 200,
        sortable: true,
        filter: true,
        floatingFilter: true
      },
      rowSelection: 'single'
    };
  }
  init(url: string) {
    this.gridOptions.onGridReady = () => {
      console.log(url);
      this.rest.get(url).subscribe(
        rowData => {
          console.log(rowData);
          this.gridOptions.api.setRowData(rowData);
          this.gridOptions.api.sizeColumnsToFit();
        },
        error => console.log(error)
      );
    };
  }

}
