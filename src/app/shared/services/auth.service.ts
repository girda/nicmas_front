import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser } from '../interfaces/user.interface';
import {tap} from 'rxjs/operators';
import {UserService} from './user.service';
import {environment} from '../../../environments/environment';
import {TechnicianService} from './technician.service';
import {IProduct} from '../interfaces/product.interface';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token = null;

  constructor(private http: HttpClient,
              private user: UserService,
              private techService: TechnicianService) {

  }

  register(user: IUser): Observable<IUser> {
    return this.http.post<IUser>('/api/auth/register', user);
  }

  login(user: IUser): Observable<{token: string, role: string, product: IProduct}> {
    const productId = localStorage.getItem('product-id');
    const reqData = {user, product_id: productId};
    return this.http.post<{token: string, role: string, product: IProduct}>(`${environment.apiUrl}/api/auth/login`, reqData)
      .pipe(
        tap(
          ({token, role, product}) => {
            console.log(product);
            this.setToken(token);
            localStorage.setItem('role', role);
            this.user.setRole(role);
            this.techService.setProduct(product);
          }
        )
      );
  }

  setToken(token: string) {
    this.token = token;
    localStorage.setItem('auth-token', token);
  }

  getToken(): string {
    return this.token;
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  logout() {
    this.setToken(null);
    this.user.setRole(null);
    this.techService.setProduct(null);
    localStorage.clear();
  }
}
