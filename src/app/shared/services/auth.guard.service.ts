import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/internal/Observable';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {of} from 'rxjs/internal/observable/of';
import {KeysService} from './keys.service';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(private auth: AuthService,
              private user: UserService,
              private router: Router,
              private keys: KeysService) {

  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    console.log(this.keys);
    console.log(this.auth.isAuthenticated());
    if (this.auth.isAuthenticated()) {
      if (this.user.getRole() === route.data.role) {
        return of(true);
      }
      this.router.navigate(['/login'], {
        queryParams: {
          incorrectRole: true
        }
      });
      return of(false);

    } else {
      this.router.navigate(['/login'], {
        queryParams: {
          accessDenied: true
        }
      });
      return of(false);
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }
}
