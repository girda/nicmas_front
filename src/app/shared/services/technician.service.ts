import {Injectable} from '@angular/core';
import {IProduct} from '../interfaces/product.interface';

@Injectable({
  providedIn: 'root'
})

export class TechnicianService {
  private currentProduct: IProduct;
  constructor() {}

  getProduct() {
    if (this.currentProduct) {
      return this.currentProduct;
    } else {
      this.currentProduct = JSON.parse(localStorage.getItem('product'));
      return this.currentProduct;
    }
  }

  setProduct(product: IProduct) {
    this.currentProduct = product;
    if (this.currentProduct) {
      localStorage.setItem('product', JSON.stringify(product));
    }
  }
}
