import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KeysService {
  role = {
    operator: 'Operator',
    controller: 'Controller',
    technician: 'Technician'
  };
}
