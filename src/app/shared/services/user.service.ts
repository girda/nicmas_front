import {Injectable} from '@angular/core';
import {IUser} from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService implements IUser {
  login;
  password;
  role = null;

  getRole(): string {
    return this.role;
  }

  setRole(role: string) {
    this.role = role;
  }

}
