import {Component} from '@angular/core';

@Component({
  selector: 'app-button-open-modal',
  templateUrl: './button-ag-grid.component.html',
  styleUrls: ['./button-ag-grid.component.css']
})
export class ButtonAgGridComponent {
  public params: any;

  agInit(params: any): void {
    this.params = params;
  }

  openModal() {
    this.params.context.componentParent.openModal(this.params);
  }
}
