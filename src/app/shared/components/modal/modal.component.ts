import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MaterialInstance, MaterialService} from '../../services/material.service';
import {ModalService} from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, AfterViewInit {
  @ViewChild('modal', {static: false}) modalRef: ElementRef;
  public modal: MaterialInstance;

  constructor(private modalService: ModalService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
    this.modalService.add(this.modal);
  }

  open() {
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

}
