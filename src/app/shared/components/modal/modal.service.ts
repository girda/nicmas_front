import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ModalService {
  private modal;

  add(modal) {
    this.modal = modal;
  }

  open() {
    this.modal.open();
  }

  close() {
    this.modal.close();
  }
}
