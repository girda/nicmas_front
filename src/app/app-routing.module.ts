import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './shared/auth/login/login.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { SiteLayoutComponent } from './shared/layouts/site-layout/site-layout.component';
import { RegisterComponent } from './shared/auth/register/register.component';
import {OperatorPageComponent} from './operator-page/operator-page.component';
import {ControllerPageComponent} from './controller-page/controller-page.component';

import {AuthGuardService} from './shared/services/auth.guard.service';
import {KeysService} from './shared/services/keys.service';
import {TechnicianPageComponent} from './technician-page/technician-page.component';


const keys = new KeysService();

const routes: Routes = [
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {path: '', redirectTo: '/login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent}
    ]
  },
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      {
        path: 'operator',
        component: OperatorPageComponent,
        canActivate: [AuthGuardService],
        data: {role: keys.role.operator}
      },
      {
        path: 'controller',
        component: ControllerPageComponent,
        canActivate: [AuthGuardService],
        data: {role: keys.role.controller}
      },
      {
        path: 'technician',
        component: TechnicianPageComponent,
        canActivate: [AuthGuardService],
        data: {role: keys.role.technician}
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
