import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './shared/auth/login/login.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { SiteLayoutComponent } from './shared/layouts/site-layout/site-layout.component';
import { RegisterComponent } from './shared/auth/register/register.component';
import { TokenInterceptor } from './shared/services/token-interceptor.service';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { ControllerPageComponent } from './controller-page/controller-page.component';
import { OperatorPageComponent } from './operator-page/operator-page.component';
import {AgGridModule} from 'ag-grid-angular';
import { ButtonAgGridComponent } from './shared/components/button-ag-grid/button-ag-grid.component';
import { ModalComponent } from './shared/components/modal/modal.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { TechnicianPageComponent } from './technician-page/technician-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AuthLayoutComponent,
    SiteLayoutComponent,
    RegisterComponent,
    LoaderComponent,
    ControllerPageComponent,
    OperatorPageComponent,
    ButtonAgGridComponent,
    ModalComponent,
    TechnicianPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgGridModule,
    ZXingScannerModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production, registrationStrategy: 'registerImmediately' })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
