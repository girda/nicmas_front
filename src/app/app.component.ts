import {Component, OnInit} from '@angular/core';
import {AuthService} from './shared/services/auth.service';
import {UserService} from './shared/services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private auth: AuthService,
              private user: UserService) {

  }

  ngOnInit() {
    const protectionToken = localStorage.getItem('auth-token');
    const protectionRole = localStorage.getItem('role');

    if (protectionToken !== null) {
      this.auth.setToken(protectionToken);
      this.user.setRole(protectionRole);
    }
    console.log(location);
    if (location.search) {
      try {
        const strProductId = location.search.split('?product_id=')[1];
        // const productId = strProductId.slice(0, strProductId.lastIndexOf(';'));
        // const serialNumber = location.search.split('serial_number-')[1];
        localStorage.setItem('product-id', location.search.split('?product_id=')[1]);
        // localStorage.setItem('serial-number', serialNumber);
      } catch (e) {
        console.log(e);
      }

    }

  }
}
